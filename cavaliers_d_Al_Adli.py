def DemVal(_type_ = "Int" , msg = "Entrez une valeur : "):
    # La fonction DemVal() permets de demander une valeur (input) d'un certain type et en affichant une certaine valeur.
    # La valeur est demandée en boucle jusqu'à ce que l'utilisatuer face ce qui est demandé.
    
    # Demande à l'utilisateur d'entrer un entier.
    # Si l'utilisateur n'entre rien, le programme est en pause.
    # Si l'utilisateur entre autre-chose qu'un entier, le programme réitère sa demande en précisant qu'il veut un entier.
    if(_type_ == "Int"):
        # loop tant que non entier
        ok = 0
        while(ok == 0):
            # Demande d'entrer un entier
            try:
                a = int(input(msg))
            # Message d'erreur (si valeur précédente non entier)
            except ValueError as RetourException:
                print("Vous devez saisir un entiers !\nRéessayez !")
                print("Erreur : ", RetourException)
            else:
               ok = 1 # Ligne atteinte uniquement si un entier est entré (validation de la condition / sortie de loop)

    # Demande à l'utilisateur d'entrer une chaîne de charactères.
    # Si l'utilisateur n'entre rien, le programme est en pause.
    # Si l'utilisateur entre autre-chose qu'une chaîne de charactères, le programme réitère sa demande en précisant qu'il veut une chaîne de caractères.
    elif(_type_ == "Str"):
        # loop tant que non chaîne de charactères
        ok = 0
        while(ok == 0):
            # Demande d'entrer une chaîne de charactères
            try:
                a = str(input(msg))
            # Message d'erreur (si valeur précédente non chaîne de charactères)
            except ValueError as RetourException:
                print("Vous devez saisir une chaîne de charactères !\nRéessayez !")
                print("Erreur : ", RetourException)
            else:
                ok = 1 # Ligne atteinte uniquement si une chaîne de charactères est entré (validation de la condition / sortie de loop)

    # Le type choisis par l'utilisateur est incorrect         
    else:
        print("Le type demandé n'est pas disponible ou n'a pas été reconnu.")
        a = "Erreur"
        
    return (a)


def initialisation():
    # Création du plateau et mise en place des pièces
    T = [["  ","| ","1 ","2 ","3 ","| "," "],
         ["- ","+ ","- ","- ","- ","+ ","-"],
         ["1 ","| ","B ","O ","B ","| ","1"],
         ["2 ","| ","O ","O ","O ","| ","2"],
         ["3 ","| ","N ","O ","N ","| ","3"],
         ["- ","+ ","- ","- ","- ","+ ","-"],
         ["  ","| ","1 ","2 ","3 ","| "," "],
        ]
    return(T)

def afficher(T):
    # Affichage du plateau
    for i in range(7): # Lignes
        for j in range(7): # Colones
            print(T[i][j], end = "")
        print("") # Changement de ligne (anulation du end = "")
    print("") # Saut de ligne (lisibilité)

def depart(T):
    # Sélection de la case de départ

    # 3 états possibles des cases (impossible de sélectionner une case d'un autre état)
    vide = "O "
    blanc = "B "
    noir = "N "

    # Les valeurs sont demandées en boucle jusqu'à ce que l'utilisatuer face ce qui est demandé.
    depart_OK = False
    while(depart_OK == False):
        j = DemVal("Int","Sur quelle colonne se situe la pièce à déplacer ? ") + 1 # Demande de la colone de départ // Incrémentation de 1 pour plus de facilité avec le tableau
        i = DemVal("Int","Sur quelle ligne se situe la pièce à déplacer ? ") + 1 # Demande de la ligne de départ // Incrémentation de 1 pour plus de facilité avec le tableau
        if(T[i][j] == blanc or T[i][j] == noir): # Vérification (La case est-elle occupée ?)
            depart_OK = True
            return(i,j)
        elif(T[i][j] == vide): # Vérification (La case est-elle vide ?)
            print("La case sélectionnée est vide.\n")
        else: # Cette situation ne devrait jamais se produire (La case n'est ni vide ni pleine)
             print("La case désignée n'est pas valide.\n")

def arrivee(T):
    # Sélection de la case d'arrivée

    # 3 états possibles des cases (impossible de sélectionner une case d'un autre état)
    vide = "O "
    blanc = "B "
    noir = "N "

    # Les valeurs sont demandées en boucle jusqu'à ce que l'utilisatuer face ce qui est demandé.
    arrivee_OK = False
    while(arrivee_OK == False):
        y = DemVal("Int","Sur quelle colonne voulez-vous déplacer la pièce ? ") + 1 # Demande de la colone d'arrivée // Incrémentation de 1 pour plus de facilité avec le tableau
        x = DemVal("Int","Sur quelle ligne voulez-vous déplacer la pièce ? ") + 1 # Demande de la ligne d'arrivée // Incrémentation de 1 pour plus de facilité avec le tableau
        print("") # Saut de ligne (lisibilité)
        if(T[x][y] == vide): # Vérification (La case est-elle vide ?)
            arrivee_OK = True
            return(x,y)
        elif(T[x][y] == blanc or T[x][y] == noir): # Vérification (La case est-elle occupée ?)
            print("La case désignée est occupée.\n")
        else: # Cette situation ne devrait jamais se produire (La case n'est ni vide ni pleine)
             print("La case désignée n'est pas valide.\n")

def verif_deplacement(i,j,x,y):
    # Vérification de la conformité du déplacement du cavalier

    # Décrémentation (annulation de l'incrémentation précédente, gênante pour cette opération)
    i -= 1
    j -= 1
    x -= 1
    y -= 1

    # Les valeurs sont demandées en boucle jusqu'à ce que l'utilisatuer face ce qui est demandé.
    deplacement_OK = False

    # Vérification pour chaque case de départ (i,j), si la case d'arrivée est valide (x,y) par test de chaque proposition pouvant être valide.
    if(j == 1 and i == 1):
        if(y == 3 and x == 2):
            deplacement_OK = True
        elif(y == 2 and x == 3):
            deplacement_OK = True
        else:
            deplacement_OK = False
    elif(j == 2 and i == 1):
        if(y == 1 and x == 3):
            deplacement_OK = True
        elif(y == 3 and x == 3):
            deplacement_OK = True
        else:
            deplacement_OK = False
    elif(j == 3 and i == 1):
        if(y == 1 and x == 2):
            deplacement_OK = True
        elif(y == 2 and x == 3):
            deplacement_OK = True
        else:
            deplacement_OK = False
    elif(j == 1 and i == 2):
        if(y == 3 and x == 1):
            deplacement_OK = True
        elif(y == 3 and x == 3):
            deplacement_OK = True
        else:
            deplacement_OK = False
    elif(j == 3 and i == 2):
        if(y == 1 and x == 1):
            deplacement_OK = True
        elif(y == 1 and x == 3):
            deplacement_OK = True
        else:
            deplacement_OK = False
    elif(j == 1 and i == 3):
        if(y == 2 and x == 1):
            deplacement_OK = True
        elif(y == 3 and x == 2):
            deplacement_OK = True
        else:
            deplacement_OK = False
    elif(j == 2 and i == 3):
        if(y == 1 and x == 1):
            deplacement_OK = True
        elif(y == 3 and x == 1):
            deplacement_OK = True
        else:
            deplacement_OK = False
    elif(j == 3 and i == 3):
        if(y == 2 and x == 1):
            deplacement_OK = True
        elif(y == 1 and x == 2):
            deplacement_OK = True
        else:
            deplacement_OK = False
    else:
        deplacement = False

    # Message d'erreur (si le déplacement n'est pas valide)
    if(deplacement_OK == False):
        print("Votre déplacement n'est pas valide !\n")
    
    return(deplacement_OK)


def echange(i,j,x,y,T):
    # Déplacement graphique des pièces

    # 3 états possibles des cases (impossible de sélectionner une case d'un autre état)
    vide = "O "
    blanc = "B "
    noir = "N "

    couleur = T[i][j] # Prélèvement de la couleur pour ne pas échanger un cavalier blanc et un noir durant l'opération
    T[i][j] = vide # Vidage de la case de départ

    # Choix de la couleur de la case d'arrivée
    if(couleur == blanc):
        T[x][y] = blanc
    elif(couleur == noir):
        T[x][y] = noir

    # Cette situation ne devrait jamais se produire (La pièce n'est ni blanche ni noire)
    else:
        print("Errreur système.\nPièce inexistante.\n")

def choix_mode():
    # Définition de l'action du programe

    # Présentation du problème
    print("Enigme des cavaliers d'Al-Adli\n\nSur un échiquier de taille 3×3, l'énigme des cavaliers d'Al-Adli consiste à inverser deux cavaliers blancs et deux cavaliers noirs.\n")

    action = DemVal("Str","Action à effectuer : (S olution, C ommencer, A utomatique, F in, H elp) : ") # Sélection de l'action            
    return(action)

def commencer():
    # La partie se déroule normalement

    print("A vous de jouer!\n")

    # 3 états possibles des cases (impossible de sélectionner une case d'un autre état)
    vide = "O "
    blanc = "B "
    noir = "N "
    
    T = initialisation() # Création du plateau et mise en place des pièces
    afficher(T) # Affichage du plateau
    compteur = 0

    # Le programe boucle tant que la partie n'est pas gagnée
    en_cours = True
    while(en_cours):
        # Vérification (Partie gagnée ?)
        if(T[2][2] == noir and T[2][4] == noir and T[4][2] == blanc and T[4][4] == blanc):
            en_cours = False
        else:
            # Boucle tant que le déplacement n'est pas valide
            deplacement_OK = False
            while(deplacement_OK == False):
                i,j = depart(T) #Sélection de la case de départ
                x,y = arrivee(T) #Sélection de la case d'arrivée
                deplacement_OK = verif_deplacement(i,j,x,y) # Vérification de la conformité du déplacement du cavalier
            echange(i,j,x,y,T) # Déplacement graphique des pièces
            compteur += 1
            afficher(T) # Affichage du plateau
    print("Voilà bien un génie des échecs !\nBravo!\nVous avez résolu l'énigme en",compteur,"coups, le minimimum étant de 16 coups.\n") # Message de félicitation

def solution():
    # Affichage de la solution
    
    print("") # Saut de ligne (lisibilité)

    # Tableau contenant la solution step by step
    S = [["1","1",";","3","2"],
         ["1","3",";","2","1"],
         ["3","2",";","1","3"],
         ["3","3",";","1","2"],
         ["3","1",";","2","3"],
         ["1","2",";","3","1"],
         ["2","3",";","1","1"],
         ["3","1",";","2","3"],
         ["1","1",";","3","2"],
         ["2","3",";","1","1"],
         ["2","1",";","3","3"],
         ["3","3",";","1","2"],
         ["1","2",";","3","1"],
         ["1","3",";","2","1"],
         ["2","1",";","3","3"],
         ["3","2",";","1","3"],
        ]
    
    # Affichage de la solution
    for i in range(16):
        for j in range(5):
            print(S[i][j]," ",end = "")
        print("") # Changement de ligne (anulation du end = "")
    print("") # Saut de ligne (lisibilité)

def automatique():
    # Le programe joue tout seul

    print("\nLa résolution de catte énigme nécessite au moins 16 coups.\n") # Saut de ligne (lisibilité)

    # Tableau contenant la solution step by step
    # Il s'agit du même tableau que dans solution(), recopié pour une meilleure accessibilité.
    S = [["1","1","3","2"],
         ["1","3","2","1"],
         ["3","2","1","3"],
         ["3","3","1","2"],
         ["3","1","2","3"],
         ["1","2","3","1"],
         ["2","3","1","1"],
         ["3","1","2","3"],
         ["1","1","3","2"],
         ["2","3","1","1"],
         ["2","1","3","3"],
         ["3","3","1","2"],
         ["1","2","3","1"],
         ["1","3","2","1"],
         ["2","1","3","3"],
         ["3","2","1","3"],
        ]

    # 3 états possibles des cases (impossible de sélectionner une case d'un autre état)
    vide = "O "
    blanc = "B "
    noir = "N "
    
    T = initialisation() # Création du plateau et mise en place des pièces
    afficher(T) # Affichage du plateau

    for a in range(16):# Mettre 13 pour afficher toutes les solutions
        j = S[a][0]
        i = S[a][1]
        y = S[a][2]
        x = S[a][3]

        # Afficher le déplacement
        print("Action : ",j,",",i," ; ",y,",",x,"\n")
        
        # Mise à jour du type des variables suivantes.
        # Jusqu'alors, il s'agissait de str trouvés dans le tableau S (si echange() appelé via automatique(). Sinon, aucun changement effectué.)
        i = int(i) + 1
        j = int(j) + 1
        x = int(x) + 1
        y = int(y) + 1
        
        echange(i,j,x,y,T) # Déplacement graphique des pièces
        afficher(T) # Affichage du plateau

def fin():
    # Le joueur souhaite quitter le programe
    
    print("J'espère te revoir bientôt.\nAu revoir !\n")
    loop = False
    arret_definitif = input("Tapez [entrer] pour fermer la fenêtre.") # Mise en pause de programe (utile uniquement dans le shell, permets de voir le message d'au-revoir)
    return(loop)

def aide():
    # Le joueur a besoin d'aide

    print("\nVotre saisie doit être effectuée en majuscule.")
    print("Vous devez taper l'un des caractères suivants :")
    print("- C dans le cas où vous souhaiter résoudre l'énigme normalement.")
    print("- S dans le cas où vous souhaitez voir la solution")
    print("- A dans le cas où vous souhaitez voir le programe résoudre l'énigme en détaillant les étapes.\n")

# Programe principal
# Se répète en boucle tant que la réponse à fin() ne contredis pas l'action
loop = True
while(loop):
    action = choix_mode() # Récupération de l'action
    
    # Sélection du comportement du programe en fonction de l'action
    if(action == "C"):
        commencer() # La partie se déroule normalement
    elif(action == "S"):
        solution() # Affichage de la solution
    elif(action == "A"):
        automatique() # Le programe joue tout seul
    elif(action == "F"):
        loop = fin() # Le joueur souhaite quitter le programe
    else:
        aide() # Le joueur a besoin d'aide
